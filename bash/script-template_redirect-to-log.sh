#!/bin/sh

{
START=`date +%s`	
echo "   -----   SCRIPTNAME Started @ `date +'%m_%d_%Y_%I-%M_%p'`   -----   "
### SCRIPT GOES HERE ###

END=`date +%s`
RUNTIME=$((END-START))
echo "   -----   SCRIPTNAME Ended @ `date +'%m_%d_%Y_%I-%M_%p'` & Finished in $RUNTIME seconds   -----   "

} 2>&1 | tee /root/Logs/SCRIPTNAME.log
