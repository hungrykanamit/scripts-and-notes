# Goal: copy line #1 from /etc/hostname to line #2 of /etc/hosts, then append 127.0.1.1 to start of line #2.

##########
printf '%s\n' '0r !head -n 1 /etc/hostname' x | ed -s /etc/hosts <<< $'1r /etc/hostname\nw'
sed -i '/127.0.0.1/{s/.*/&\n127.0.1.1	/;:a;n;ba}' /etc/hosts
sed -i '2N;s/\n/      /' /etc/hosts
