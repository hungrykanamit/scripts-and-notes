# Steg Passwd Store
_A guide on using steganography, encryption, rsa keys and git to make a personal and multi-node password store._

## Install Prerequisites
```
sudo dnf install -y gpg2 pass steghide jhead
```

## generate a new gpg key
```
gpg2 --full-generate-key
```
* copy pub key outputted from `gpg2 --full-generate-key`
* eg: `00DD855XXXXXXXXXXXXXXXXXXXXXXXXXXXXX25AX`
* *Note:* if you ever need to pull this up again type:
  ```
  gpg2 --list-keys
  ```

## Enter passwords into the store
* to enter passwords, do so like this: `pass insert <service>/<username>`
* *Note:* in zsh, auto complete does not work for `pass`. It does with fine with bash.

## Write data to be embedded into file.
* Write a file named `steg.txt` (can be named anything) and put your pub key and master password in it, anything else wanted as well.

## Generate import pass-phrase
* This is for the encryption of the key during transfer. Copy the output to the clipboard and write it to `steg.txt`
  ```
  gpg2 --armor --gen-random 1 20
  ```

## Encrypt the key for exporting
* Make a file named `mykey.sec.asc` (can be named anything) which stores the secret keys. You need to enter both the import pass-phrase just generated above and the password generated during the creation.
  ```
  gpg2 --armor --export-secret-keys 00DD855XXXXXXXXXXXXXXXXXXXXXXXXXXXXX25AX | gpg2 --armor --symmetric --output mykey.sec.asc
  ```
* To import this on Linux, use the following:
  ```
  gpg2 --decrypt mykey.sec.asc > mykey.sec.decrypted.asc
  gpg2 --import mykey.sec.decrypted.asc
  ```

## Prep the data for embedding
* Copy contents of `mykey.sec.asc` into `steg.txt` as we can only embed one file into a photo with steghide.

## Choose a photo to embed
_You must also choose a file that is memorable, not too obvious, and has an Exif timestamp so as to camouflage the photo more and not have it jump out when looking at the modified date, this will be expanded on at a later step._
  * In order to get a list of all files in a dir with an Exif timestamp, use the following:
    ```
    jhead -ft -exonly -exifmap <dir_with_photos>/* | grep -v Map
    ```
  * You will need to keep a copy this file in order to have a copy of the Exif data after embedding the data.
    ```
    cp <path>/<photo>.<ext> <path>/<photo>_copy.<ext>
    ```

## Embed the file into the photo
```
steghide embed -cf <path>/<photo>.<ext> -ef ./steg.txt
```
* To extract the data, use:
  ```
  steghide extract -sf <path>/<photo>.<ext>
  ```

## Transfer the Exif data from the Copy
* Use the copied photo as the Exif data source for transfer to the embedded photo, then remove the copy, and restore file metadata from Exif data for all files in the dir
  ```
  jhead -te -exonly <path>/<photo>_copy.<ext> <path>/<photo>.<ext>
  rm -rf <path>/<photo>_copy.<ext>
  jhead -ft -exonly <path>/*
  ```

## Prep Android phone
Install `openkeychain` & `password store` on Android
* Both avaiable on the Play Store and on Fdroid

## Move the key to the phone
* Move `mykey.sec.asc` to an OTG usb to securely send to phone (email can work, if trusted)
* Make sure that these files are removed from your pc after this stage
  ```
  rm -rf steg.txt mykey.sec.asc
  ```
* Insert the usb to the phone & start openkeychain and select import from file
* Find the file on the usb and select it.
* When it asks for a password, use the import passphrase, and select import

# Open passwordstore
