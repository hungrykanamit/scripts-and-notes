# Use the chef-client cookbook to set up your node to run chef-client periodically
#### Get the chef-client cookbook

`cd ~/chef-repo/`

Create a file named Berksfile and add these contents to create a configuration file that tells Berkshelf which cookbooks you want and where they're located

`nano Berksfile` # write the following into the file:

```
source 'https://supermarket.chef.io'
cookbook 'chef-client'
```

#### run berks install to download the chef-client cookbook and its dependencies

`berks install`

#### Run berks upload to upload the chef-client cookbook and its dependencies to Chef server

`berks upload --no-ssl-verify`
