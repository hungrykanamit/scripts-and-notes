
# Bootstrap chefnode1 using password authentication from your chef workstation
> knife bootstrap ADDRESS --ssh-user USER --sudo --use-sudo-password --node-name NODE-NAME --run-list 'recipe[learn_chef_httpd]'

```
knife bootstrap 23.239.210.104 --ssh-user root --sudo --use-sudo-password --node-name chefnode1 --run-list 'recipe[learn_chef_httpd]'`
```
## Verify your node was associated with your Chef server
```
knife node list
knife node show chefnode1
```
