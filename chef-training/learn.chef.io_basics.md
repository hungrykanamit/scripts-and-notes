
# Install chefDK
`curl https://omnitruck.chef.io/install.sh | sudo bash -s -- -P chefdk -c stable -v 0.18.30
`

### Make a MOTD (message of the day)

1. make a repo dir called chef-repo

2. make a hello.rb recipe inside of chef-repo

```
file '/tmp/motd' do
  content 'hello chef'
end
```

#### Apply the hello.rb recipe
`chef-client /root/chef-repo/hello.rb`

file is now also in /tmp/motd, show the file contents with:
`more /tmp/motd`

#### to delete /tmp/motd write the following to a new recipe named goodbye.rb then apply it

```
file '/tmp/motd' do
  action :delete
end
```

Apply the goodbye.rb recipe: `chef-client /root/chef-repo/goodbye.rb`

# Make a basic webserver
### The following installs apache2, enables then starts the apache2 service, and then creates a custom homepage (index.html)

make a webserver.rb recipe inside of chef-repo then apply it

```
package 'httpd'

service 'httpd' do
  action [:enable, :start]
end

file '/var/www/html/index.html' do
  content '<html>
  <body>
    <h1>hello world</h1>
  </body>
</html>'
end
```

### Apply the webserver.rb recipe
`chef-client /root/chef-repo/webserver.rb`

### Make a cookbooks dir
`mkdir /root/chef-repo/cookbooks`

Run the following chef generate cookbook command to generate a cookbook named learn_chef_httpd

`chef generate cookbook /root/chef-repo/cookbooks/learn_chef_httpd`

Run this command to generate the HTML file for our home page

`chef generate template /root/chef-repo/cookbooks/learn_chef_httpd index.html`

copy the contents of the HTML file from your recipe to the new HTML file, `/root/chef-repo/cookbooks/learn_chef_httpd/templates/index.html.erb`

```
<html>
  <body>
    <h1>hello world</h1>
  </body>
</html>
```

Write out the recipe

`nano /root/chef-repo/cookbooks/learn_chef_httpd/recipes/default.rb`

```
package 'httpd'

service 'httpd' do
  action [:enable, :start]
end

template '/var/www/html/index.html' do
  source 'index.html.erb'
end
```

### Now run the cookbook. To do so, we use the chef-client command and specify what's called the run-list.

`chef-client --local-mode --runlist 'recipe[learn_chef_httpd]'`

###### The run-list lets you specify which recipes to run, and the order in which to run them.

# Install chefDK on workstation (RHEL)
```
wget https://packages.chef.io/files/stable/chefdk/1.4.3/el/7/chefdk-1.4.3-1.el7.x86_64.rpm
```
Check chef version

`chef -v`

Make a new dir for the workstation

`mkdir Documents/Training/learn-chef`

Verify git is installed

`git --version`

## For instructions on installing chef server go to: https://docs.chef.io/install_server.html

Download the package from `https://downloads.chef.io/chef-server/`. Then upload it to `/tmp` of server using:

`scp ~/Downloads/chef-server-core-*.rpm root@xxx.xxx.xxx.xxx:/tmp`

#### Now do the following while in an ssh session on the chef server:
Install the rpm

`rpm -Uvh /tmp/chef-server-core-*.rpm`

Start all of the services:

`chef-server-ctl reconfigure`

Create an administrator:
###### if placing files in the example dirs, they must be made first using `mkdir /var/chef`
> chef-server-ctl user-create USER_NAME FIRST_NAME LAST_NAME EMAIL 'PASSWORD' --filename FILE_NAME

```
chef-server-ctl user-create chefadmin chef admin chefadmin@clamshelldata.com 'clamshell' --filename /var/chef/chefadmin.pem
```

Create an organization:
> chef-server-ctl org-create short_name 'full_organization_name' --association_user user_name --filename ORGANIZATION-validator.pem

```
chef-server-ctl org-create clamshelldata 'Clamshell Data LLC' --association_user chefadmin --filename /var/chef/clamshelldata-validator.pem
```

###### If server will not be using a doamin, and only is using IPv4, do the following:
```
nano /etc/hostname #replace hostname with IPv4
rebbot
```

Install chef-manage (This provides a web interface to your Chef server.)

```
chef-server-ctl install chef-manage
chef-server-ctl reconfigure
chef-manage-ctl reconfigure
#:wq
#yes
```

# Set up your .chef directory on your workstation
1. In a web browser on your workstation, got to the domain or ip of the chef server.
2. Sign in, click the administrtion tab, select the organization, on the side bar click starter kit, then download starter kit
3. Move the chef-starter.zip file to the desired location and extract
4. Verify that the chef-repo/.chef/knife.rb has the correct chef_server_url (if not, change it)
```
cd chef-repo/
knife ssl fetch
knife ssl check
```

#### Get the learn_chef_httpd cookbook from GitHub
```
cd ~/chef-repo/cookbooks
git clone https://github.com/learn-chef/learn_chef_httpd.git
```

Upload the learn_chef_httpd cookbook to your Chef server

`knife cookbook upload learn_chef_httpd`

Run the knife cookbook list command to verify

`knife cookbook list`

# Bootstrap node1 using password authentication
> knife bootstrap ADDRESS --ssh-user USER --sudo --use-sudo-password --node-name NODE-NAME --run-list 'recipe[learn_chef_httpd]'

```
knife bootstrap 23.239.210.104 --ssh-user root --sudo --use-sudo-password --node-name chefnode1 --run-list 'recipe[learn_chef_httpd]'
```

Verify your node was associated with your Chef server
```
knife node list
knife node show chefnode1
```

Add template code to your HTML

`nano cookbooks/learn_chef_httpd/templates/index.html.erb` #change this file to the following:

```
<html>
  <body>
    <h1>hello from <%= node['fqdn'] %></h1>
  </body>
</html>
```

#### The <%= %> syntax enables you to provide placeholders in your template file.

Update your cookbook's version metadata

`nano cookbooks/learn_chef_httpd/metadata.rb # change version to '0.2.0'`

Upload your cookbook to the Chef server

`knife cookbook upload learn_chef_httpd`

Run the cookbook on your node with knife ssh-user
> knife ssh 'name:NODE-NAME' 'sudo chef-client' --ssh-user USER --attribute ipaddress

```
knife ssh 'name:chefnode1' 'sudo chef-client' --ssh-user root --attribute ipaddress
```

Assign an owner to the home page
`nano cookbooks/learn_chef_httpd/recipes/default.rb` # change contents to the following:

```
#
# Cookbook Name:: learn_chef_httpd
# Recipe:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.
package 'httpd'

service 'httpd' do
  action [:enable, :start]
end

template '/var/www/html/index.html' do
  source 'index.html.erb'
  mode '0644'
  owner 'web_admin'
  group 'web_admin'
end
```

### Apply the changes to your node
Update the cookbook's version metadata.

`nano cookbooks/learn_chef_httpd/metadata.rb # change version to '0.3.0'`

Upload the cookbook to Chef server

`knife cookbook upload learn_chef_httpd`

Run chef-client on your node

`knife ssh 'name:chefnode1' 'sudo chef-client' --ssh-user root --attribute ipaddress`

# Resolve the failure
Add the web_admin group and user to the system by modifying your default recipe

`nano cookbooks/learn_chef_httpd/recipes/default.rb` # change contents to the following:

```
#
# Cookbook Name:: learn_chef_httpd
# Recipe:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.
package 'httpd'

service 'httpd' do
  action [:enable, :start]
end

group 'web_admin'

user 'web_admin' do
  group 'web_admin'
  system true
  shell '/bin/bash'
end

template '/var/www/html/index.html' do
  source 'index.html.erb'
  mode '0644'
  owner 'web_admin'
  group 'web_admin'
end
```

Update the cookbook's version metadata

`nano cookbooks/learn_chef_httpd/metadata.rb # change version to '0.3.1'`

Upload the cookbook to Chef server

`knife cookbook upload learn_chef_httpd`

Run chef-client on your node

`knife ssh 'name:chefnode1' 'sudo chef-client' --ssh-user root --attribute ipaddress`

# Use the chef-client cookbook to set up your node to run chef-client periodically

Get the chef-client cookbook

`cd ~/chef-repo/`
`
create a file named Berksfile and add these contents to create a configuration file that tells Berkshelf which cookbooks you want and where they're located

`nano Berksfile` # write the following into the file:

```
source 'https://supermarket.chef.io'
cookbook 'chef-client'
```

# Run berks install to download the chef-client cookbook and its dependencies

`berks install`

Run berks upload to upload the chef-client cookbook and its dependencies to Chef server

`berks upload --no-ssl-verify`

# Create a role
First, ensure you have a directory named `~/chef-repo/roles`

`cd ~/chef-repo`

`nano roles/web.json` # add the following to the file:

```
{
   "name": "web",
   "description": "Web server role.",
   "json_class": "Chef::Role",
   "default_attributes": {
     "chef_client": {
       "interval": 300,
       "splay": 60
     }
   },
   "override_attributes": {
   },
   "chef_type": "role",
   "run_list": ["recipe[chef-client::default]",
                "recipe[chef-client::delete_validation]",
                "recipe[learn_chef_httpd::default]"
   ],
   "env_run_lists": {
   }
}
```
Run the following knife role from file command to upload your role to the Chef server

`knife role from file roles/web.json`

You can run knife role list to verify

`knife role list`

You can also run knife role show to view the role's details

`knife role show web`

### set your node's run-list
`knife node run_list set chefnode1 "role[web]"`

You can run the knife node show to verify

`knife node show chefnode1 --run-list`

# Run chef-client
```
knife ssh 'role:web' 'sudo chef-client' --ssh-user root --attribute ipaddress
```
##### You can run the knife status command to display a brief summary of the nodes on your Chef server, including the time of the most recent successful chef-client run.

`knife status 'role:web' --run-list`

# Extra Steps

### Delete the node from the Chef server
```
knife node delete NODE-NAME --yes
knife client delete NODE-NAME --yes
```

### Delete your cookbook from the Chef server

`knife cookbook delete learn_chef_httpd --all --yes`

##### If you omit the --all argument, you'll be prompted to select which version to delete.

### Delete the role from the Chef server

`knife role delete web --yes`

### Delete the RSA private key from your node

`sudo rm /etc/chef/client.pem`
