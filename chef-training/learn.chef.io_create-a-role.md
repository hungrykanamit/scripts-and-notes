# Create a role
### First, ensure you have a directory named `~/chef-repo/roles`

`cd ~/chef-repo`
`nano roles/web.json` # add the following to the file:

```
{
   "name": "web",
   "description": "Web server role.",
   "json_class": "Chef::Role",
   "default_attributes": {
     "chef_client": {
       "interval": 300,
       "splay": 60
     }
   },
   "override_attributes": {
   },
   "chef_type": "role",
   "run_list": ["recipe[chef-client::default]",
                "recipe[chef-client::delete_validation]",
                "recipe[learn_chef_httpd::default]"
   ],
   "env_run_lists": {
   }
}
```

#### run the following knife role from file command to upload your role to the Chef server

`knife role from file roles/web.json`

#### you can run knife role list to verify

`knife role list`

#### You can also run knife role show to view the role's details

`knife role show web`

#### set your node's run-list

`knife node run_list set chefnode1 "role[web]"`

#### you can run the knife node show to verify

`knife node show chefnode1 --run-list`

#### Run chef-client

`knife ssh 'role:web' 'sudo chef-client' --ssh-user root --attribute ipaddress`
