# For instructions on installing chef server go to https://docs.chef.io/install_server.html

### Download the package from https://downloads.chef.io/chef-server/. Then upload it to `/tmp` of server with:
`scp ~/Downloads/chef-server-core-*.rpm root@xxx.xxx.xxx.xxx:/tmp`

### Now do the following while in an ssh session on the chef server:
Install the rpm

`rpm -Uvh /tmp/chef-server-core-*.rpm`

Start all of the services:

`chef-server-ctl reconfigure`

Create an administrator:
###### if placing files in the example dirs, they must be made first. `mkdir /var/chef`

> chef-server-ctl user-create USER_NAME FIRST_NAME LAST_NAME EMAIL 'PASSWORD' --filename FILE_NAME

```
chef-server-ctl user-create chefadmin chef admin chefadmin@clamshelldata.com 'clamshell' --filename /var/chef/chefadmin.pem
```

Create an organization:
> chef-server-ctl org-create short_name 'full_organization_name' --association_user user_name --filename ORGANIZATION-validator.pem

```
chef-server-ctl org-create clamshelldata 'Clamshell Data LLC' --association_user chefadmin --filename /var/chef/clamshelldata-validator.pem
```

###### If server will not be using a doamin, and only is using IPv4, do the following:
```
nano /etc/hostname #replace hostname with IPv4
rebbot
```

Install chef-manage (This provides a web interface to your Chef server.)

```
chef-server-ctl install chef-manage
chef-server-ctl reconfigure
chef-manage-ctl reconfigure
#:wq
#yes
```
