#!/bin/bash

#  Determine the size of the new swap file in megabytes and multiply by `1024` to determine the number of blocks. For example, the block size of a `5 GB` swap file is `5120000`.

dd if=/dev/zero of=/swapfile bs=1024 count=2048000
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile

# Set swappiness from `30` to `10`
echo "/swapfile swap swap defaults 0 0" >> /etc/fstab
echo "10" > /proc/sys/vm/swappiness

# Set cache pressure from `100` to `50`
echo "vm.swappiness = 10" >> /etc/sysctl.conf
sudo sysctl vm.vfs_cache_pressure=50
echo "vm.vfs_cache_pressure = 50" >> /etc/sysctl.conf

yum -y update
yum -y localinstall http://mirror.nethserver.org/nethserver/nethserver-release-7.rpm
nethserver-install
# nethserver should now be running, if it is not or you saw this error:
# "Failed to start nethserver-system-init.service: Operation refused, unit nethserver-system-init.service may be requested by dependency only.
# See system logs and 'systemctl status nethserver-system-init.service' for details."
# Then run the following:
# `cat /lib/systemd/system/nethserver-system-init.service`
# Check what the "ExecStart=" is, it should be this: /sbin/e-smith/signal-event system-init
# Run the "ExecStart=" and nethserver should now be running.

yum -y upgrade
yum -y update --enablerepo=stephdl
yum -y install perl-Email-Valid python-inotify jwhois #fail2ban fail2ban-systemd 
yum -y install http://mirror.de-labrusse.fr/NethServer/7/x86_64/nethserver-stephdl-1.0.6-1.ns7.sdl.noarch.rpm
yum -y install nethserver-fail2ban --enablerepo=stephdl
yum -y update selinux-policy*

config setprop fail2ban BanTime -1 FindTime 600 BanLocalNetwork enabled
signal-event nethserver-fail2ban-save

echo "[INFO]" > /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "Here are some commands that you may find useful when using fail2ban." >> /etc/fail2ban/info.txt
echo "For more info on fail2ban vist this page: https://linux.die.net/man/1/fail2ban-client" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "For more information on nethserver-fail2ban, visit this page:" >> /etc/fail2ban/info.txt
echo "https://wiki.nethserver.org/doku.php?id=module:fail2ban" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "1) Check the Fal2Ban Status:" >> /etc/fail2ban/info.txt
echo "fail2ban-client status" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "2) Detailed information about sshd jail:" >> /etc/fail2ban/info.txt
echo "fail2ban-client status sshd" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "3) Show all IPs which are banned by shorewall:" >> /etc/fail2ban/info.txt
echo "fail2ban-listban" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "4) Unbanning an IP address:" >> /etc/fail2ban/info.txt
echo "fail2ban-unban <IP>" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "5) Show current nethserver-fail2ban config:" >> /etc/fail2ban/info.txt
echo "config show fail2ban" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "6) Edit current nethserver-fail2ban config:" >> /etc/fail2ban/info.txt
echo "config setprop fail2ban <function> <value> #eg: FindTime 600 (or) BanLocalNetwork enabled" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "7) Save current nethserver-fail2ban config:" >> /etc/fail2ban/info.txt
echo "signal-event nethserver-fail2ban-save" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
printf "%s\n""\e[0;32m" && echo "nethserver-fail2ban has been installed and preformed a basic configuration" && printf "\e[0m\n"
echo " "
cat /etc/fail2ban/info.txt
