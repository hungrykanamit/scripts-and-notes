#!/bin/bash

# Variables
# ----------
## Paths:
rc_skins_dir=/usr/share/roundcubemail/skins
rc_plugins_dir=/usr/share/roundcubemail/plugins
enigma='/var/www/enigma/home'
## Strings:
rcmail='$rcmail_config'
q_failed_attempts_s=\'failed_attempts\'
q_failed_release_s=\'failed_release\'
q_https_s=\'https\'
q_publickey_s=\'recaptcha_publickey\'
q_privatekey_s=\'recaptcha_privatekey\'
q_recaptcha_log_s=\'recaptcha_log\'
above_failed_attempts="Number of failed logins before reCAPTCHA is shown"
above_failed_release="Release IP after how many minutes"
above_https="Use HTTPS for reCAPTCHA"
above_publickey="Public key for reCAPTCHA"
above_privatekey="Private key for reCAPTCHA"
above_recaptcha_log="Log events"
config='$config'
q_enigma_pgp_homedir_s=\'enigma_pgp_homedir\'
q_enigma_sign_all_s=\'enigma_sign_all\'
q_enigma_encrypt_all_s=\'enigma_encrypt_all\'
q_enigma_attach_pubkey_s=\'enigma_attach_pubkey\'
enigma_esc=\/var\/www\/enigma\/home
q_enigma_sign_all=true
q_enigma_encrypt_all=true
q_enigma_attach_pubkey=true
above_enigma_pgp_homedir="Must be writeable by PHP process"
above_enigma_sign_all="Enable signing all messages by default"
above_enigma_encrypt_all="Enable encrypting all messages by default"
above_enigma_attach_pubkey="Enable attaching a public key to all messages by default"

# Script
# ----------
yum install -y unzip

# Install mabola-blue skin
cd "$rc_skins_dir"
wget https://github.com/EstudioNexos/mabola-blue/archive/master.zip
unzip master.zip
rm -rf master.zip
mv mabola-blue-master mabola-blue

# # Install persistent_login (leave out after consideration)
# cd "$rc_plugins_dir"
# wget https://github.com/mfreiholz/Roundcube-Persistent-Login-Plugin/archive/master.zip
# unzip master.zip
# rm -rf master.zip
# mv persistent_login-master persistent_login

# # Install SpamAssassin-User-Prefs-SQL (skipping for now)
# # (http://notes.sagredo.eu/en/qmail-notes-185/roundcube-plugins-35.html#sauserprefs)

# Install rcguard
cd "$rc_plugins_dir"
wget https://github.com/dsoares/rcguard/archive/master.zip
unzip master.zip
rm -rf master.zip
mv rcguard-master rcguard
chown -R root.apache rcguard
chmod -R o-rx rcguard
mv rcguard/config.inc.php.dist rcguard/config.inc.php
printf "\n\e[0;32mYou have to obtain a key from \e[4;34mhttp://www.google.com/recaptcha\e[0m\n"
printf "\e[0;33mYou may have to disable 'Domain Name Validation'\e[0m\n"
printf "\n\e[0;36mHow many failed logins allowed before reCAPTCHA is shown? (default=5)\e[0m\n"
read failed_attempts
printf "\n\e[0;36mRelease IP after how many minutes since last failed attempt? (default=30)\e[0m\n"
read failed_release
printf "\n\e[0;36mUse HTTPS for reCAPTCHA? (default=false)\e[0m\n"
read https
printf "\n\e[0;36mWhat is your recaptcha publickey (Site key)?\e[0m\n"
read recaptcha_publickey
printf "\n\e[0;36mWhat is your recaptcha privatekey (Secret key)?\e[0m\n"
read recaptcha_privatekey
printf "\n\e[0;36mWould you like to log recaptcha events? (default=false)\e[0m\n"
read recaptcha_log
q_failed_attempts=\'${failed_attempts}\'
q_failed_release=\'${failed_release}\'
q_https=\'${https}\'
q_recaptcha_publickey=\'${recaptcha_publickey}\'
q_recaptcha_privatekey=\'${recaptcha_privatekey}\'
q_recaptcha_log=\'${recaptcha_log}\'
failed_attempts_after="${rcmail}[${q_failed_attempts_s}] = ${q_failed_attempts};"
failed_release_after="${rcmail}[${q_failed_release_s}] = ${q_failed_release};"
https_after="${rcmail}[${q_https_s}] = ${q_https};"
publickey_after="${rcmail}[${q_publickey_s}] = ${q_recaptcha_publickey};"
privatekey_after="${rcmail}[${q_privatekey_s}] = ${q_recaptcha_privatekey};"
recaptcha_log_after="${rcmail}[${q_recaptcha_log_s}] = ${q_recaptcha_log};"
sed -i "/$above_failed_attempts/{n;s/.*/$failed_attempts_after/}" "$rc_plugins_dir"/rcguard/config.inc.php
sed -i "/$above_failed_release/{n;s/.*/$failed_release_after/}" "$rc_plugins_dir"/rcguard/config.inc.php
sed -i "/$above_https/{n;s/.*/$https_after/}" "$rc_plugins_dir"/rcguard/config.inc.php
sed -i "/$above_publickey/{n;s/.*/$publickey_after/}" "$rc_plugins_dir"/rcguard/config.inc.php
sed -i "/$above_privatekey/{n;s/.*/$privatekey_after/}" "$rc_plugins_dir"/rcguard/config.inc.php
sed -i "/$above_recaptcha_log/{n;s/.*/$recaptcha_log_after/}" "$rc_plugins_dir"/rcguard/config.inc.php
pw=(`cat /root/.my.cnf | grep 'password' | cut -d "=" -f2`)
db=(`mysql -uroot -p$pw -Dmysql -e 'show databases;' | grep 'roundcube'`)
mysql -uroot -p$pw -D$db -e 'CREATE TABLE `rcguard` (`ip` VARCHAR(40) NOT NULL,`first` DATETIME NOT NULL,`last` DATETIME NOT NULL,`hits` INT(10) NOT NULL,PRIMARY KEY (`ip`),INDEX `last_index` (`last`),INDEX `hits_index` (`hits`)) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;'
printf "\n\e[0;32mGoogle recaptcha has been configured for RoundCube & a new table 'rcguard' has been created in the '"$db"' database\e[0m\n\n"

# Context Menu
cd "$rc_plugins_dir"
wget https://github.com/JohnDoh/Roundcube-Plugin-Context-Menu/archive/master.zip
unzip master.zip
rm -rf master.zip
mv roundcube-contextmenu-master contextmenu
chown -R root.apache contextmenu
chmod -R o-rx contextmenu

# Enigma
cd "$rc_plugins_dir"
wget https://github.com/roundcube/roundcubemail/archive/master.zip
unzip master.zip roundcubemail-master/plugins/enigma/*
rm -rf master.zip
mv roundcubemail-master/plugins/enigma enigma
rm -rf roundcubemail-master
cp -p enigma/config.inc.php.dist enigma/config.inc.php
mkdir -p "$enigma"
q_enigma_pgp_homedir=\'${enigma_esc}\'
enigma_pgp_homedir_after="${config}[${q_enigma_pgp_homedir_s}] = ${q_enigma_pgp_homedir};"
enigma_sign_all_after="${config}[${q_enigma_sign_all_s}] = ${q_enigma_sign_all};"
enigma_encrypt_all_after="${config}[${q_enigma_encrypt_all_s}] = ${q_enigma_encrypt_all};"
enigma_attach_pubkey_after="${config}[${q_enigma_attach_pubkey_s}] = ${q_enigma_attach_pubkey};"
sed -i "/$above_enigma_pgp_homedir/{n;s/.*/ABRACADABRA/}" "$rc_plugins_dir"/enigma/config.inc.php
sed -i "s|ABRACADABRA|$enigma_pgp_homedir_after|" "$rc_plugins_dir"/enigma/config.inc.php
sed -i "/$above_enigma_sign_all/{n;s/.*/$enigma_sign_all_after/}" "$rc_plugins_dir"/enigma/config.inc.php
sed -i "/$above_enigma_encrypt_all/{n;s/.*/$enigma_encrypt_all_after/}" "$rc_plugins_dir"/enigma/config.inc.php
sed -i "/$above_enigma_attach_pubkey/{n;s/.*/$enigma_attach_pubkey_after/}" "$rc_plugins_dir"/enigma/config.inc.php
cd "$enigma"/../..
chown -R root:apache enigma
chmod -R g+w enigma
systemctl stop httpd.service
systemctl start httpd.service
cd "$rc_plugins_dir"/enigma/lib
wget http://download.pear.php.net/package/Crypt_GPG-1.6.2.tgz
tar xzf Crypt_GPG-1.6.2.tgz
rm -rf Crypt_GPG-1.6.2.tgz
ln -s Crypt_GPG-1.6.2/Crypt
chown -R root:apache Crypt*
chmod -R g+w Crypt*

# Enable plugins
config setprop roundcubemail PluginsList additional_message_headers,archive,attachment_reminder,contextmenu,emoticons,enigma,hide_blockquote,identity_select,managesieve,markasjunk,newmail_notifier,rcguard,redundant_attachments,vcard_attachments,zipdownload
# Update roundcubemail
signal-event nethserver-roundcubemail-update
# Exit Info
printf "\n\e[0;32mGoogle recaptcha has been configured for RoundCube & a new table 'rcguard' has been created in the '"$db"' database\e[0m\n\n"
printf "\e[0;33mIf there are errors in the recaptcha, you may have to disable 'Domain Name Validation' on this page: \e[4;33mhttp://www.google.com/recaptcha\e[0m\n"
printf "\n\e[0;32mFor more info on Enigma visit this page \e[4;34mhttps://kolabian.wordpress.com/2015/10/13/enigma-plugin-pgp-encryption/\e[0m\n"
