#!/bin/bash

yum -y update
yum -y localinstall http://mirror.nethserver.org/nethserver/nethserver-release-7.rpm
nethserver-install
# nethserver should now be running, if it is not or you saw this error:
# "Failed to start nethserver-system-init.service: Operation refused, unit nethserver-system-init.service may be requested by dependency only.
# See system logs and 'systemctl status nethserver-system-init.service' for details."
# Then run the following:
# `cat /lib/systemd/system/nethserver-system-init.service`
# Check what the "ExecStart=" is, it should be this: /sbin/e-smith/signal-event system-init
# Run the "ExecStart=" and nethserver should now be running.
