#!/bin/bash

yum -y upgrade
yum -y install http://mirror.de-labrusse.fr/NethServer/7/x86_64/nethserver-stephdl-1.0.6-1.ns7.sdl.noarch.rpm
yum -y update --enablerepo=stephdl

# Install crontabmanager
printf "\n\e[0;32mInstalling crontabmanager, for more info visit: \e[4;34mhttps://wiki.nethserver.org/doku.php?id=crontab\e[0m\n"
yum -y install nethserver-crontabmanager --enablerepo=stephdl

# Install Clamscan
printf "\n\e[0;32mInstalling Clamscan, for more info visit: \e[4;34mhttps://wiki.nethserver.org/doku.php?id=clamscan\e[0m\n"
yum -y install nethserver-clamscan

# Install Yum-cron
printf "\n\e[0;32mInstalling Yum-cron, for more info visit: \e[4;34mhttps://wiki.nethserver.org/doku.php?id=yum-cron\e[0m\n"
yum -y install nethserver-yum-cron
config setprop yum-cron randomWait 30
signal-event nethserver-yum-cron-update

yum -y install perl-Email-Valid python-inotify jwhois #fail2ban fail2ban-systemd
yum -y install nethserver-fail2ban --enablerepo=stephdl
yum -y update selinux-policy*

config setprop fail2ban BanTime -1 FindTime 600 BanLocalNetwork enabled
signal-event nethserver-fail2ban-save

echo "[INFO]" > /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "Here are some commands that you may find useful when using fail2ban." >> /etc/fail2ban/info.txt
echo "For more info on fail2ban vist this page: https://linux.die.net/man/1/fail2ban-client" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "For more information on nethserver-fail2ban, visit this page:" >> /etc/fail2ban/info.txt
echo "https://wiki.nethserver.org/doku.php?id=module:fail2ban" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "1) Check the Fal2Ban Status:" >> /etc/fail2ban/info.txt
echo "fail2ban-client status" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "2) Detailed information about sshd jail:" >> /etc/fail2ban/info.txt
echo "fail2ban-client status sshd" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "3) Show all IPs which are banned by shorewall:" >> /etc/fail2ban/info.txt
echo "fail2ban-listban" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "4) Unbanning an IP address:" >> /etc/fail2ban/info.txt
echo "fail2ban-unban <IP>" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "5) Show current nethserver-fail2ban config:" >> /etc/fail2ban/info.txt
echo "config show fail2ban" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "6) Edit current nethserver-fail2ban config:" >> /etc/fail2ban/info.txt
echo "config setprop fail2ban <function> <value> #eg: FindTime 600 (or) BanLocalNetwork enabled" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "7) Save current nethserver-fail2ban config:" >> /etc/fail2ban/info.txt
echo "signal-event nethserver-fail2ban-save" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
printf "%s\n""\e[0;32m" && echo "nethserver-fail2ban has been installed and preformed a basic configuration" && printf "\e[0m\n"
echo " "
cat /etc/fail2ban/info.txt
