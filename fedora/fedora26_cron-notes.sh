# Read cron logs
sudo tail -f /var/log/cron

# cron does not like world writable files, all you have to do it set 0644 (rw-r–r)
chmod 0644 /etc/cron.d/tom

# you can make a symbolic link likr this
ln -s /home/tom/Git/scripts-and-notes/git/git-run_linux.sh

#note that files cannot have extensions in the name.
mv git-run.sh git-run
