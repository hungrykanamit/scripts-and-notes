#!/bin/bash

# Variables:
# ----------

## Paths:
git_repo=Shell_Colors

# Script:
# ----------

## nanorc
cd ~
mkdir "$git_repo"
cd "$git_repo"
git clone https://github.com/nanorc/nanorc.git
cd nanorc
make install
cd ~
for i in ~/.nano/syntax/*.nanorc; do
  echo "include $i" >> ~/.nanorc
done
rm -rf "$git_repo"

## Vim Awesome Version
# git clone --depth=1 https://github.com/amix/vimrc.git ~/.vim_runtime
# sh ~/.vim_runtime/install_awesome_vimrc.sh

## Vim Basic Version
git clone --depth=1 https://github.com/amix/vimrc.git ~/.vim_runtime
sh ~/.vim_runtime/install_basic_vimrc.sh
