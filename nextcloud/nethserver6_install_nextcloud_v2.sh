##### How to install NextCloud on top of Nethserver 6.8 #####

### Step 1 - Install PHP 5.5 on Nethserver ###
yum update && yum install epel-release
# rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-6.rpm
rpm -Uvh https://centos6.iuscommunity.org/ius-release.rpm
#yum --enablerepo=ius install libevent2 libmemcached-last-libs 
#nano /etc/yum.repos.d/remi.repo
## Edit the following ##
# [remi-php55]
# ...
# enabled=0
## To ##
# [remi-php55]
# ...
# enabled=1
yum --enablerepo=remi install php php-common
yum upgrade
yum --enablerepo=remi install php-fpm php-mysql php-pgsql php-pecl-mongo php-sqlite php-pecl-memcache php-pecl-memcached php-gd php-mbstring php-mcrypt php-xml php-pecl-apc php-cli php-pear php-pdo php-imap
yum upgrade
yum clean all

### Step 2 - Create phpinfo file ###
nano /var/www/html/phpinfo.php
#<?php
#
#//Show all information, defaults to INFO_ALL
#phpinfo();
#
#?>
init 6 #I do not know how to restart httpd.service in centos6, but a reboot works
## After the reboot, you can go to xxx.xxx.xxx.xxx/phpinfo.php to check php settings ##

### Step 3 - Downlaod the latest version of NextCloud ###
mkdir /root/NextCloud/
cd /root/NextCloud/
wget https://download.nextcloud.com/server/releases/nextcloud-10.0.1.zip
unzip nextcloud-10.0.1.zip
cp SuiteCRM-7.7.8 nextcloud
cp nextcloud /var/www/html/

### Step 4 - Set php mem to 256 ###
db configuration setprop php MemoryLimit 256
signal-event nethserver-php-save

### Step 5 - change permisions while in /var/www/html/nextcloud ###
sudo chown -R apache:apache .
sudo chmod -R 755 .
sudo chmod -R 775 console.php 
init 6 #I do not know how to restart httpd.service in centos6, but a reboot works
## After the reboot, you can go to xxx.xxx.xxx.xxx/nextcloud/index.php/login to create an admin ##
