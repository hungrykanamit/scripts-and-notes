# Notes
#As of writing this (04/08/2017) mariadb would not install on ubuntu 16.04.2 so to do the install I ran a clean install of ubuntu 14.04 and ran the following:
#	apt-get -y update
#	apt-get -y upgrade
#	apt-get -y install apache2 apache2-utils
#	systemctl start apache2
#	systemctl enable apache2
#	apt-get -y install libclone-perl libmldbm-perl libnet-daemon-perl libsql-statement-perl libdata-dump-perl libipc-sharedcache-perl libwww-perl tinyca libaio1 libcgi-fast-perl libcgi-pm-perl libdbd-mysql-perl libdbi-perl libencode-locale-perl libfcgi-perl libhtml-parser-perl libhtml-tagset-perl libhtml-template-perl libhttp-date-perl libhttp-message-perl libio-html-perl liblwp-mediatypes-perl libmysqlclient20 libterm-readkey-perl libtimedate-perl liburi-perl mysql-common bsd-mailx
#	apt-get -y install mariadb-common mariadb-server-core-10.0 mariadb-client-core-10.0 mariadb-client-10.0 #mariadb-server-10.0 mariadb-test-data mariadb-test
#	apt-get update
#	apt-get dist-upgrade
#	apt-get install update-manager-core
#	do-release-upgrade
#follow prompts

# Requirements:
#ubuntu 16.04
#512 MB Ram
#3 GB Disk

# Pre-Config
sed -i "s|#precedence ::ffff:0:0/96  100|precedence ::ffff:0:0/96  100|g" /etc/gai.conf
SSL_CONF="/etc/apache2/sites-available/nextcloud_ssl_domain_self_signed.conf"
HTTP_CONF="/etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf"
NCREPO="https://download.nextcloud.com/server/releases/"
NCVERSION=$(curl -s $NCREPO | tac | grep unknown.gif | sed 's/.*"nextcloud-\([^"]*\).zip.sha512".*/\1/;q')
STABLEVERSION="nextcloud-$NCVERSION"
HTML=/var/www
NCPATH=$HTML/nextcloud
GPGDIR=/tmp/gpg
NCDATA=/var/ncdata
OpenPGP_fingerprint='28806A878AE423A28372792ED75899B9A724937A'
SCRIPTS=/var/scripts
STATIC="https://gitlab.com/hungrykanamit/clamshell-data/raw/master/Nextcloud/Scripts/"
mkdir /var/ncdata

# Install Script Dependencies
apt-get -y update
apt-get -y upgrade
apt-get -y install python-pip
pip install --upgrade pip
cp /etc/apt/sources.list /etc/apt/sources.list.backup
rm -rf /var/cache/apt/archives/*.deb
apt-get -y update
apt-get -y upgrade
apt-get -y install apache2 apache2-utils
systemctl start apache2
systemctl enable apache2
apt-get -y install libclone-perl libmldbm-perl libnet-daemon-perl libsql-statement-perl libdata-dump-perl libipc-sharedcache-perl libwww-perl tinyca libaio1 libcgi-fast-perl libcgi-pm-perl libdbd-mysql-perl libdbi-perl libencode-locale-perl libfcgi-perl libhtml-parser-perl libhtml-tagset-perl libhtml-template-perl libhttp-date-perl libhttp-message-perl libio-html-perl liblwp-mediatypes-perl libmysqlclient20 libterm-readkey-perl libtimedate-perl liburi-perl mysql-common bsd-mailx
apt-get -y install mariadb-common mariadb-server-core-10.0 mariadb-client-core-10.0 mariadb-client-10.0 #mariadb-server-10.0 mariadb-test-data mariadb-test
rm -rf /var/cache/apt/archives/*.deb
apt-get -y install libapache2-mod-php7.0 #libapache2-mod-fcgid 
apt-get -y install php7.0-common php7.0-mysql php7.0-intl php7.0-mcrypt php7.0-ldap php7.0-imap php7.0-cli php7.0-gd php7.0-pgsql php7.0-json php7.0-sqlite3 php7.0-curl php7.0-xml php7.0-zip php7.0-mbstring php-smbclient php-imagick php-ftp php-exif php7.0-gmp php-apcu
apt-get -y update
apt-get -y upgrade
rm -rf /var/cache/apt/archives/*.deb
apt-get -y install unzip
apt-get -y install libav-tools libavdevice-dev libavformat-dev libavfilter-dev libavcodec-dev libswscale-dev libavutil-dev
apt-get -y update
apt-get -y upgrade
rm -rf /var/cache/apt/archives/*.deb
a2enmod ssl headers

# Download and validate Nextcloud package
wget -q $NCREPO/$STABLEVERSION.zip -P $HTML
mkdir -p $GPGDIR
wget -q $NCREPO/$STABLEVERSION.zip.asc -P $GPGDIR
chmod -R 600 $GPGDIR
gpg --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys "$OpenPGP_fingerprint"
gpg --verify $GPGDIR/$STABLEVERSION.zip.asc $HTML/$STABLEVERSION.zip
rm -r $GPGDIR

# Extract package
unzip -q $HTML/$STABLEVERSION.zip -d $HTML
rm $HTML/$STABLEVERSION.zip

## Secure permissions
#run "Nextcloud_Set-File-Permissions.sh" (found in Nexcloud home folder)
chown -R www-data:www-data $NCDATA

# Install Nextcloud
cd $NCPATH
sudo -u www-data php occ maintenance:install \
    --data-dir "$NCDATA" \
    --database "mysql" \
    --database-name "nextcloud_db" \
    --database-user "root" \
    --database-pass "nextcloud" \
    --admin-user "ncadmin" \
    --admin-pass "nextcloud"
sudo -u www-data php $NCPATH/occ status
cd

# Prepare cron.php to be run every 15 minutes
crontab -u www-data -l | { cat; echo "*/15  *  *  *  * php -f $NCPATH/cron.php > /dev/null 2>&1"; } | crontab -u www-data -

# Change values in php.ini (increase max file size)
# max_execution_time
sed -i "s|max_execution_time = 30|max_execution_time = 3500|g" /etc/php/7.0/apache2/php.ini
# max_input_time
sed -i "s|max_input_time = 60|max_input_time = 3600|g" /etc/php/7.0/apache2/php.ini
# memory_limit
sed -i "s|memory_limit = 128M|memory_limit = 512M|g" /etc/php/7.0/apache2/php.ini
# post_max
sed -i "s|post_max_size = 8M|post_max_size = 1100M|g" /etc/php/7.0/apache2/php.ini
# upload_max
sed -i "s|upload_max_filesize = 2M|upload_max_filesize = 1000M|g" /etc/php/7.0/apache2/php.ini

# Increase max filesize (expects that changes are made in /etc/php/7.0/apache2/php.ini)
sed -i 's/  php_value upload_max_filesize 511M/# php_value upload_max_filesize 511M/g' $NCPATH/.htaccess
sed -i 's/  php_value post_max_size 511M/# php_value post_max_size 511M/g' $NCPATH/.htaccess
sed -i 's/  php_value memory_limit 512M/# php_value memory_limit 512M/g' $NCPATH/.htaccess

## Install Figlet
#apt install figlet -y

# Generate /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
touch "/etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf"
echo "<VirtualHost *:80>" > /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    DocumentRoot /var/www/nextcloud" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    <Directory /var/www/nextcloud>" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    Options Indexes FollowSymLinks" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    AllowOverride All" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    Require all granted" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    Satisfy Any" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    </Directory>" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    <IfModule mod_dav.c>" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    Dav off" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    </IfModule>" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    <Directory "/var/ncdata">" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    # just in case if .htaccess gets disabled" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    Require all denied" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    </Directory>" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    SetEnv HOME /var/www/nextcloud" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    SetEnv HTTP_HOME /var/www/nextcloud" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "</VirtualHost>" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf

# Generate /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
touch "/etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf"
echo "<VirtualHost *:443>" > /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    Header add Strict-Transport-Security: XXXXXX" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
NEW_STRING='"max-age=15768000;includeSubdomains"'
sed -i "s/XXXXXX/$NEW_STRING/" /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    SSLEngine on" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    DocumentRoot /var/www/nextcloud" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    <Directory /var/www/nextcloud>" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    Options Indexes FollowSymLinks" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    AllowOverride All" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    Require all granted" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    Satisfy Any" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    </Directory>" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    <IfModule mod_dav.c>" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    Dav off" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    </IfModule>" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    <Directory "/var/ncdata">" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    # just in case if .htaccess gets disabled" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    Require all denied" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    </Directory>" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    SetEnv HOME /var/www/nextcloud" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    SetEnv HTTP_HOME /var/www/nextcloud" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "### LOCATION OF CERT FILES ###" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    SSLCertificateFile /etc/ssl/certs/ssl-cert-snakeoil.pem" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "    SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf
echo "</VirtualHost>" >> /etc/apache2/sites-available/nextcloud_http_domain_self_signed.conf

# Enable new config
a2ensite nextcloud_ssl_domain_self_signed.conf
a2ensite nextcloud_http_domain_self_signed.conf
a2dissite default-ssl
service apache2 restart

## Set config values
# Experimental apps
sudo -u www-data php $NCPATH/occ config:system:set appstore.experimental.enabled --value="true"
# Default mail server as an example (make this user configurable?)
sudo -u www-data php $NCPATH/occ config:system:set mail_smtpmode --value="smtp"
sudo -u www-data php $NCPATH/occ config:system:set mail_smtpauth --value="1"
sudo -u www-data php $NCPATH/occ config:system:set mail_smtpport --value="465"
sudo -u www-data php $NCPATH/occ config:system:set mail_smtphost --value="smtp.gmail.com"
sudo -u www-data php $NCPATH/occ config:system:set mail_smtpauthtype --value="LOGIN"
sudo -u www-data php $NCPATH/occ config:system:set mail_from_address --value="www.techandme.se"
sudo -u www-data php $NCPATH/occ config:system:set mail_domain --value="gmail.com"
sudo -u www-data php $NCPATH/occ config:system:set mail_smtpsecure --value="ssl"
sudo -u www-data php $NCPATH/occ config:system:set mail_smtpname --value="www.techandme.se@gmail.com"
sudo -u www-data php $NCPATH/occ config:system:set mail_smtppassword --value="vinr vhpa jvbh hovy"

# Install Libreoffice Writer to be able to read MS documents.
sudo apt install --no-install-recommends libreoffice-writer -y
apt-get -y update
apt-get -y upgrade
rm -rf /var/cache/apt/archives/*.deb

# Nextcloud apps
CONVER=$(curl -s https://api.github.com/repos/nextcloud/contacts/releases/latest | grep "tag_name" | cut -d\" -f4 | sed -e "s|v||g")
CONVER_FILE=contacts.tar.gz
CONVER_REPO=https://github.com/nextcloud/contacts/releases/download
CALVER=$(curl -s https://api.github.com/repos/nextcloud/calendar/releases/latest | grep "tag_name" | cut -d\" -f4 | sed -e "s|v||g")
CALVER_FILE=calendar.tar.gz
CALVER_REPO=https://github.com/nextcloud/calendar/releases/download
sudo -u www-data php $NCPATH/occ config:system:set preview_libreoffice_path --value="/usr/bin/libreoffice"

# Download and install Calendar
wget -q $CALVER_REPO/v$CALVER/$CALVER_FILE -P $NCPATH/apps
tar -zxf $NCPATH/apps/$CALVER_FILE -C $NCPATH/apps
cd $NCPATH/apps
rm $CALVER_FILE

# Enable Calendar
sudo -u www-data php $NCPATH/occ app:enable calendar

# Download and install Contacts
wget -q $CONVER_REPO/v$CONVER/$CONVER_FILE -P $NCPATH/apps
tar -zxf $NCPATH/apps/$CONVER_FILE -C $NCPATH/apps
cd $NCPATH/apps
rm $CONVER_FILE

# Enable Contacts
sudo -u www-data php $NCPATH/occ app:enable contacts

# Install packages for Webmin
apt install -y zip perl libnet-ssleay-perl openssl libauthen-pam-perl libpam-runtime libio-pty-perl apt-show-versions python

# Install Webmin
sed -i '$a deb http://download.webmin.com/download/repository sarge contrib' /etc/apt/sources.list
wget -q http://www.webmin.com/jcameron-key.asc -O- | sudo apt-key add -
apt update -q2
apt install webmin -y

# Confirm Nextcloud apps installs
whiptail --title "Which apps/programs do you want to install?" --checklist --separate-output "" 10 40 3 \
"Calendar" "              " on \
"Contacts" "              " on \
"Webmin" "              " on 2>results

### Skipped ###
# Change roots .bash_profile
#wget -q $STATIC/change-root-profile.sh -P $SCRIPTS

### Skipped ###
# Change $UNIXUSER .bash_profile
#wget -q $STATIC/change-ncadmin-profile.sh -P $SCRIPTS

### Changed ###
# Welcome message after login (change in $HOME/.profile
wget -q $STATIC/instruction.sh -P $SCRIPTS

### Not Touched Yet, Must Update ###
# Get nextcloud-startup-script.sh
wget -q $GITHUB_REPO/nextcloud-startup-script.sh -P $SCRIPTS

# Get update.sh to automate updates
wget -q $STATIC/update.sh -P $SCRIPTS

# Get script to clear history on every login
wget -q $STATIC/history.sh -P $SCRIPTS

# Get script for Redis
wget -q $STATIC/redis-server-ubuntu16.sh -P $SCRIPTS

# Make $SCRIPTS excutable
chmod +x -R $SCRIPTS
chown root:root -R $SCRIPTS

# Install Redis
bash $SCRIPTS/redis-server-ubuntu16.sh
#rm $SCRIPTS/redis-server-ubuntu16.sh

# Upgrade
apt update -q2
apt full-upgrade -y

# Remove LXD (always shows up as failed during boot)
apt purge lxd -y

# Cleanup
apt autoremove -y
apt autoclean

# Set permissions for last time (./data/.htaccess has wrong permissions otherwise)
bash $SCRIPTS/Nextcloud_Set-File-Permissions.sh

# Shutdown, next boot is for Clent setup
echo "Installation done, system will now shutdown so a template can be made..."
shutdown -h now

exit 0

---------------







apt-get -y update
apt-get -y upgrade

#chown www-data /var/www/html/ -R
#apt-get -y install mysql-server
#mysql_secure_installation

#-------------
#-------------
rm -rf /var/cache/apt/archives/*.deb 
apt-get -y install libreoffice

#/etc/init.d/apache2 restart

---
# The following optional packages for Nextcloud 11 have not been installed:
#PHP module memcached
#PHP module redis
#PHP module pcntl
