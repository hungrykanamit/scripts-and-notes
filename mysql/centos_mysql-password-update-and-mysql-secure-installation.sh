# mysql password update & mysql_secure_installation
sudo yum -y update
sudo yum -y install expect
read -p "Type the password you want root to use for MySQL here: " MYSQLPASS
echo "MySQL is being secured, this may take a moment..."
SECURE_MYSQL=$(expect -c "
set timeout 10
spawn mysql_secure_installation
expect \"Enter current password for root:\"
#send \"CURRENT-PASSWD\r\" (if there is a passwd set, use this and comment out below)
send \"\r\"
#expect \"Would you like to setup VALIDATE PASSWORD plugin?\"
#send \"n\r\"
expect \"Change the password for root ?\"
send \"y\r\"
send \"$MYSQLPASS\r\"
send \"$MYSQLPASS\r\"
expect \"Remove anonymous users?\"
send \"y\r\"
expect \"Disallow root login remotely?\"
send \"y\r\"
expect \"Remove test database and access to it?\"
send \"y\r\"
expect \"Reload privilege tables now?\"
send \"y\r\"
expect eof
")
echo "$SECURE_MYSQL"
sudo yum -y remove expect

# MySQL change passwd for OTHERUSER
#mysql -u root mysql -p$MYSQLPASS -e "update mysql.user set password=PASSWORD('$MYSQLPASS') where User='OTHERUSER';"
#mysql -u root -p$MYSQLPASS -e "flush privileges;"

sudo systemctl restart mysqld.service
sudo systemctl restart mariadb.service

echo " "
echo "MySQL has been secured and the passwords for root & oc-ncadmin have been set to: $MYSQLPASS"
echo "Please make a record of this"
