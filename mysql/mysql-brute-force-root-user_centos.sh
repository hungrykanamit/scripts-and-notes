systemctl stop mysqld
systemctl set-environment MYSQLD_OPTS="--skip-grant-tables"
systemctl start mysqld
#systemctl status mysqld
mysql -u root
USE mysql;
UPDATE mysql.user SET authentication_string=PASSWORD('NEWPASSWD') WHERE User='root';
FLUSH PRIVILEGES;
quit
systemctl stop mysqld
systemctl unset-environment MYSQLD_OPTS
systemctl start mysqld
