#!/bin/bash

/etc/init.d/mysql stop
sleep 5
mysqld_safe --skip-grant-tables &
sleep 10
mysql -u root -e "use mysql;"
mysql -u root mysql -e "update user set password=PASSWORD("1") where User='root';"
mysql -u root -e "flush privileges;"
/etc/init.d/mysql stop
/etc/init.d/mysql start
mysql -u root -p1 -e "SET PASSWORD FOR root@localhost=PASSWORD('')"
mysql -u root -e "SET PASSWORD FOR root@127.0.0.1=PASSWORD('')"
mysql -u root -e "flush privileges;"
read -p "Type the password you want root to use for MySQL here: " NEWPASSWD
mysqladmin -u root password $NEWPASSWD
echo 'MySQL Password updated successfully for root user.'
