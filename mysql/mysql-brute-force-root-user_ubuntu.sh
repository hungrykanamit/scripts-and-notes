service mysql start
cd /var/run
cp -rp ./mysqld ./mysqld.bak
service mysql stop
mv ./mysqld.bak ./mysqld
sudo mysqld_safe --skip-grant-tables &
mysql -u root
mysql -u root -e "use mysql;"
mysql -u root -e "update user set authentication_string=PASSWORD('NEWPASSWD') where User='root' and Host='localhost';"
mysql -u root -e "FLUSH PRIVILEGES;"
quit
service mysql stop
service mysql start
