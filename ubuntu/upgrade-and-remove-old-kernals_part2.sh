#!/bin/bash

# Cleanup 2
printf "%s\n""\e[0;32m" && echo "Cleaning Up Unused Kernals" && printf "\e[0m\n"
CLEARBOOT=$(dpkg -l linux-* | awk '/^ii/{ print $2}' | grep -v -e ''"$(uname -r | cut -f1,2 -d"-")"'' | grep -e '[0-9]' | xargs sudo apt -y purge)
echo "$CLEARBOOT"
find /root "/home/$UNIXUSER" -type f \( -name '*.sh*' -o -name '*.html*' -o -name '*.tar*' -o -name '*.zip*' \) -delete

# Check
#dpkg --get-selections | grep linux-image #(Shows all kernel versions)
#sudo apt-get -y remove --purge linux-image-4.4.0-72-generic
#apt -y update
#apt -y full-upgrade

# Reboot 2
printf "%s\n""\e[0;32m" && echo "System Will Now Reboot" && printf "\e[0m\n"
reboot
