#!/bin/bash

## Configure PHP to a more useable level ##
cp /etc/php.ini /etc/php.ini.bak
sed -i "s/memory_limit = 128M/memory_limit = 256M/" /etc/php.ini
sed -i "s/post_max_size = 8M/post_max_size = 50M/" /etc/php.ini
sed -i "s/upload_max_filesize = 2M/upload_max_filesize = 50M/" /etc/php.ini

## Make a PHPinfo page
echo "<?php" > /var/www/html/phpinfo.php
echo "//Show all information, defaults to INFO_ALL" >> /var/www/html/phpinfo.php
echo "phpinfo();" >> /var/www/html/phpinfo.php
echo "?>" >> /var/www/html/phpinfo.php

## restart apache ##
systemctl restart httpd
