#!/bin/bash

yum -y upgrade
yum -y install firewalld fail2ban fail2ban-systemd
yum update -y selinux-policy*

cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
sed -i "s|# \[DEFAULT\]|\[DEFAULT\]|" /etc/fail2ban/jail.local
sed -i "s/# bantime = 3600/bantime = -1/" /etc/fail2ban/jail.local
sed -i "s|# \[sshd\]|\[sshd\]|" /etc/fail2ban/jail.local
sed -i "s/# enabled = true/enabled = true/" /etc/fail2ban/jail.local
sed -i "s/bantime  = 600/bantime = -1/" /etc/fail2ban/jail.local
sed -i "s/maxretry = 5/maxretry = 3/" /etc/fail2ban/jail.local
sed -i "s/backend = auto/backend = systemd/" /etc/fail2ban/jail.local

systemctl enable firewalld
systemctl start firewalld
firewall-cmd --zone=public --permanent --add-service=http
firewall-cmd --zone=public --permanent --add-service=https
firewall-cmd --permanent --add-port=22/tcp
firewall-cmd --permanent --add-port=25/tcp
firewall-cmd --permanent --add-port=53/tcp
firewall-cmd --permanent --add-port=53/udp
firewall-cmd --permanent --add-port=67/udp
firewall-cmd --permanent --add-port=69/udp
firewall-cmd --permanent --add-port=110/tcp
firewall-cmd --permanent --add-port=123/udp
firewall-cmd --permanent --add-port=143/tcp
firewall-cmd --permanent --add-port=465/tcp
firewall-cmd --permanent --add-port=587/tcp
firewall-cmd --permanent --add-port=980/tcp
firewall-cmd --permanent --add-port=980/udp
firewall-cmd --permanent --add-port=993/tcp
firewall-cmd --permanent --add-port=995/tcp
firewall-cmd --permanent --add-port=4190/tcp
#firewall-cmd --permanent --add-port=21/tcp
#firewall-cmd --permanent --add-port=389/tcp
#firewall-cmd --permanent --add-port=636/tcp
#firewall-cmd --permanent --add-port=3306/tcp
#firewall-cmd --permanent --add-port=10053/udp
firewall-cmd --reload
systemctl restart firewalld
systemctl enable fail2ban
systemctl start fail2ban

echo "[INFO]" > /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "Here are some commands that you may find useful when using fail2ban." >> /etc/fail2ban/info.txt
echo "For more info on fail2ban vist this page: https://linux.die.net/man/1/fail2ban-client" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "For more info on firewalld vist this page: https://goo.gl/WvEZwq" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "1) Check the Fal2Ban Status:" >> /etc/fail2ban/info.txt
echo "fail2ban-client status" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "2) Detailed information about sshd jail:" >> /etc/fail2ban/info.txt
echo "fail2ban-client status sshd" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "3) Tracking Failed login entries:" >> /etc/fail2ban/info.txt
echo "cat /var/log/secure | grep 'Failed password'" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "4) Checking the banned IPs by Fail2Ban:" >> /etc/fail2ban/info.txt
echo "iptables -L -n" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
echo "5) Unbanning an IP address:" >> /etc/fail2ban/info.txt
echo "fail2ban-client set sshd unbanip xxx.xxx.xxx.xxx" >> /etc/fail2ban/info.txt
echo " " >> /etc/fail2ban/info.txt
printf "%s\n""\e[0;32m" && echo "fail2ban and firewalld have been installed and configured" && printf "\e[0m\n"
echo " "
cat /etc/fail2ban/info.txt

### Check the Fal2Ban Status ###
## fail2ban-client status ##

### Detailed information about sshd jail ###
## fail2ban-client status sshd ##

### Tracking Failed login entries ###
## cat /var/log/secure | grep 'Failed password' ##

### Checking the banned IPs by Fail2Ban ###
## iptables -L -n ##

### Unbanning an IP address ###
## fail2ban-client set sshd unbanip IPADDRESS ##
