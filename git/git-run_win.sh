#!/bin/bash

# Variables:
# ----------

## Names:
  script=git-run
## Paths:
  win_home=/cygdrive/c/Users/p2736134
  win_roaming="$win_home"/AppData/Roaming/Microsoft/Windows
  cd_Profile_XML_Sort="$win_home"/Documents/Scripts/profile-xml_sort
  cd_Via_Git_and_Chef_Procedures="$win_home"/VIA-Git-and-Chef-Procedures
  cd_VIA_Team_Notes="$win_home"/VIA-Team-Notes
  cd_VIA_Portable_Apps="$win_roaming"/Start\ Menu/Programs/Portable\ Apps/VIA-Portable-Apps
  cd_Downloads="$win_home"/Downloads
  cd_VIA_Tools="$cd_Downloads"/VIA-Tools
  cd_backups="$win_home"/Documents/Backups
  cd_Documents="$win_home"/Documents
  cd_Public="$win_home"/Public
  cd_Linux_Subsystem=/home/P2736134
  cd_Portable_Repo="$win_roaming"/Start\ Menu/Programs/Portable\ Apps/Portable\ Repo
  cd_temp=/cygdrive/c/Temp
  log=git-run.log
## Error Messages:
  fatal_1='fatal: The remote end hung up unexpectedly'
  fatal_2='fatal: Could not read from remote repository.'
  fatal_3='fatal: unable to connect a socket (Connection timed out)'
## Colors:
  ### Reset Color:
    Color_Off='\e[0m\n'       # Text Reset
  ### Regular Colors:
    Black='\e[0;30m'        # Black
    Red='\e[0;31m'          # Red
    Green='\e[0;32m'        # Green
    Yellow='\e[0;33m'       # Yellow
    Blue='\e[0;34m'         # Blue
    Purple='\e[0;35m'       # Purple
    Cyan='\e[0;36m'         # Cyan
    White='\e[0;37m'        # White
  ### Underline:
    UBlack='\e[4;30m'       # Black
    URed='\e[4;31m'         # Red
    UGreen='\e[4;32m'       # Green
    UYellow='\e[4;33m'      # Yellow
    UBlue='\e[4;34m'        # Blue
    UPurple='\e[4;35m'      # Purple
    UCyan='\e[4;36m'        # Cyan
    UWhite='\e[4;37m'       # White

# Functions:
# ----------

empty_recycle_bin() {
  /cygdrive/c/Users/p2736134/Documents/Scripts/empty_recycle-bin.bat &>/dev/null
}

spinner_downloading() {
  pid=$!
  spin='━\|/'
  i=0
  while [ ! -s download*.zip ]
  do
    i=$(( (i+1) %4 ))
    printf "\r[${spin:$i:1}] Please wait while VIA-Tools are downloaded..."
    sleep .1
  done
}

unzip_to_new_dirs() {
  for zip in *.zip
  do
    dirname=`echo $zip | sed 's/\.zip$//'`
    if mkdir "$dirname"
    then
      if cd "$dirname"
      then
        unzip ../"$zip"
        cd ..
        rm -rf $zip
      else
        printf "   \n${Red}Could not unpack ${URed}"$zip"${Red} - cd failed${Color_Off}\n"
      fi
    else
      printf "   \n${Red}Could not unpack ${URed}"$zip"${Red} - mkdir failed${Color_Off}\n"
    fi
  done
}

diff_dirs() {
  d=(`ls "$cd_VIA_Tools"`)
  for dirs in "${d[@]}"
  do
    if diff -rq "$cd_VIA_Tools"/"$dirs" "$cd_VIA_Portable_Apps"/"$dirs"
    then
      true
    else
      cd "$cd_VIA_Tools"
      rsync -auv * "$cd_VIA_Portable_Apps"
    fi
  done
}

Download_VIA_Tools() {
  start_Download_VIA_Tools=`date +%s`
  printf "   \n${Yellow}Downloading VIA-Tools, ${UYellow}This Window will lose focus to web browser${Color_Off}\n"
  cd "$cd_Downloads"
  cygstart "https://chalk.charter.com/pages/downloadallattachments.action?pageId=111701739" & spinner_downloading
  printf "   \n\n${Blue}VIA-Tools have been downloaded${Color_Off}\n"
  printf "   ${Green}Merging VIA-Tools into git repo${Color_Off}\n"
  mkdir -p VIA-Tools
  mv download*.zip VIA-Tools/
  cd "$cd_VIA_Tools"
  unzip download*.zip
  rm -rf download*.zip
  unzip_to_new_dirs
  diff_dirs
  cd ..
  rm -rf VIA-Tools/
  end=`date +%s`
  runtime_Download_VIA_Tools=$((end-start_Download_VIA_Tools))
  printf "\n${UPurple}   -----   Download_VIA_Tools finished in $runtime_Download_VIA_Tools seconds   -----   ${Color_Off}\n"
}

Profile_XML_Sort() {
  start_Profile_XML_Sort=`date +%s`
  printf "   \n${Green}Preforming "$script" on ${UGreen}Profile_XML_Sort${Color_Off}\n"
  cd "$cd_Profile_XML_Sort"
  if [[ $(git remote show origin) =~ "out of date" ]]
  then
    printf "      \n${Red}☠ | Remote is out of date with ${URed}Profile_XML_Sort${Color_Off}\n"
    rm -rf "$cd_backups"/Profile_XML_Sort_*.zip
    git pull -v
  elif [[ $(git status) =~ "added to commit" ]]
  then
    printf "      \n${Red}☠ | ${URed}Profile_XML_Sort${Red} is out of date with Remote${Color_Off}\n"
    rm -rf "$cd_backups"/Profile_XML_Sort_*.zip
    git add -v .
    git commit -m "$(date '+%F_%H-%M')"
    git push -v --progress
    if [[ $(grep "$fatal_1" "$cd_temp"/"$script".log) ]]
    then
      printf "   \n${Yellow}"$script" encountered the following error with ${UYellow}Profile_XML_Sort${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_1}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date '+%F_%H-%M'` & exited unexpectedly during Profile_XML_Sort after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_2" "$cd_temp"/"$script".log) ]]
    then
      printf "   \n${Yellow}"$script" encountered the following error with ${UYellow}Profile_XML_Sort${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_2}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date '+%F_%H-%M'` & exited unexpectedly during Profile_XML_Sort after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_3" "$cd_temp"/"$script".log) ]]
    then
      printf "   \n${Yellow}"$script" encountered the following error with ${UYellow}Profile_XML_Sort${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_3}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date '+%F_%H-%M'` & exited unexpectedly during Profile_XML_Sort after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    else
      end=`date +%s`
      runtime_Profile_XML_Sort=$((end-start_Profile_XML_Sort))
      printf "\n${UPurple}   -----   Profile_XML_Sort finished in $runtime_Profile_XML_Sort seconds   -----   ${Color_Off}\n"
    fi
  else
    printf "      \n${Blue}✓ | ${UBlue}Profile_XML_Sort${Blue} is up to date${Color_Off}\n"
  fi
}

Profile_XML_Sort_check() {
  cd "$cd_backups"
  if ls Profile_XML_Sort_*.zip 1> /dev/null 2>&1
  then
    printf "      \n${Blue}✓ | ${UBlue}Profile_XML_Sortt_*.zip${Blue} exists${Color_Off}\n"
  else
    printf "      \n${Red}☠ | ${URed}Profile_XML_Sortt_*.zip${Red} is missing, creating now${Color_Off}\n"
    zip -r Profile_XML_Sort_$(date '+%F').zip "$cd_Profile_XML_Sort"/
  fi
}

Via_Git_and_Chef_Procedures() {
  start_Via_Git_and_Chef_Procedures=`date +%s`
  printf "   \n${Green}Preforming "$script" on ${UGreen}Via_Git_and_Chef_Procedures${Color_Off}\n"
  cd "$cd_Via_Git_and_Chef_Procedures"
  if [[ $(git remote show origin) =~ "out of date" ]]
  then
    printf "      \n${Red}☠ | Remote is out of date with ${URed}Via_Git_and_Chef_Procedures${Color_Off}\n"
    rm -rf "$cd_backups"/Via_Git_and_Chef_Procedures_*.zip
    git pull -v
  elif [[ $(git status) =~ "added to commit" ]]
  then
    printf "      \n${Red}☠ | ${URed}Via_Git_and_Chef_Procedures${Red} is out of date with Remote${Color_Off}\n"
    rm -rf "$cd_backups"/Via_Git_and_Chef_Procedures_*.zip
    git add -v .
    git commit -m "$(date '+%F_%H-%M')"
    git push -v --progress
    if [[ $(grep "$fatal_1" "$cd_temp"/"$script".log) ]]
    then
      printf "   \n${Yellow}"$script" encountered the following error with ${UYellow}Via_Git_and_Chef_Procedures${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_1}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date '+%F_%H-%M'` & exited unexpectedly during Via_Git_and_Chef_Procedures after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_2" "$cd_temp"/"$script".log) ]]
    then
      printf "   \n${Yellow}"$script" encountered the following error with ${UYellow}Via_Git_and_Chef_Procedures${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_2}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date '+%F_%H-%M'` & exited unexpectedly during Via_Git_and_Chef_Procedures after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_3" "$cd_temp"/"$script".log) ]]
    then
      printf "   \n${Yellow}"$script" encountered the following error with ${UYellow}Via_Git_and_Chef_Procedures${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_3}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date '+%F_%H-%M'` & exited unexpectedly during Via_Git_and_Chef_Procedures after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    else
      end=`date +%s`
      runtime_Via_Git_and_Chef_Procedures=$((end-start_Via_Git_and_Chef_Procedures))
      printf "\n${UPurple}   -----   Via_Git_and_Chef_Procedures finished in $runtime_Via_Git_and_Chef_Procedures seconds   -----   ${Color_Off}\n"
    fi
  else
    printf "      \n${Blue}✓ | ${UBlue}Via_Git_and_Chef_Procedures${Blue} is up to date${Color_Off}\n"
  fi
}

Via_Git_and_Chef_Procedures_check() {
  cd "$cd_backups"
  if ls Via_Git_and_Chef_Procedures_*.zip 1> /dev/null 2>&1
  then
    printf "      \n${Blue}✓ | ${UBlue}Via_Git_and_Chef_Procedures_*.zip${Blue} exists${Color_Off}\n"
  else
    printf "      \n${Red}☠ | ${URed}Via_Git_and_Chef_Procedures_*.zip${Red} is missing, creating now${Color_Off}\n"
    zip -r Via_Git_and_Chef_Procedures_$(date '+%F').zip "$cd_Via_Git_and_Chef_Procedures"/
  fi
}

VIA_Team_Notes() {
  start_VIA_Team_Notes=`date +%s`
  printf "   \n${Green}Preforming "$script" on ${UGreen}VIA_Team_Notes${Color_Off}\n"
  cd "$cd_VIA_Team_Notes"
  if [[ $(git remote show origin) =~ "out of date" ]]
  then
    printf "      \n${Red}☠ | Remote is out of date with ${URed}VIA_Team_Notes${Color_Off}\n"
    rm -rf "$cd_backups"/VIA_Team_Notes_*.zip
    git pull -v
  elif [[ $(git status) =~ "added to commit" ]]
  then
    printf "      \n${Red}☠ | ${URed}VIA_Team_Notes${Red} is out of date with Remote${Color_Off}\n"
    rm -rf "$cd_backups"/VIA_Team_Notes_*.zip
    git add -v .
    git commit -m "$(date '+%F_%H-%M')"
    git push -v --progress
    if [[ $(grep "$fatal_1" "$cd_temp"/"$script".log) ]]
    then
      printf "   \n${Yellow}"$script" encountered the following error with ${UYellow}VIA_Team_Notes${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_1}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date '+%F_%H-%M'` & exited unexpectedly during VIA_Team_Notes after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_2" "$cd_temp"/"$script".log) ]]
    then
      printf "   \n${Yellow}"$script" encountered the following error with ${UYellow}VIA_Team_Notes${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_2}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date '+%F_%H-%M'` & exited unexpectedly during VIA_Team_Notes after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_3" "$cd_temp"/"$script".log) ]]
    then
      printf "   \n${Yellow}"$script" encountered the following error with ${UYellow}VIA_Team_Notes${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_3}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date '+%F_%H-%M'` & exited unexpectedly during VIA_Team_Notes after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    else
      end=`date +%s`
      runtime_VIA_Team_Notes=$((end-start_VIA_Team_Notes))
      printf "\n${UPurple}   -----   VIA_Team_Notes finished in $runtime_VIA_Team_Notes seconds   -----   ${Color_Off}\n"
    fi
  else
    printf "      \n${Blue}✓ | ${UBlue}VIA_Team_Notes${Blue} is up to date${Color_Off}\n"
  fi
}

VIA_Team_Notes_check() {
  cd "$cd_backups"
  if ls VIA_Team_Notes_*.zip 1> /dev/null 2>&1
  then
    printf "      \n${Blue}✓ | ${UBlue}VIA_Team_Notest_*.zip${Blue} exists${Color_Off}\n"
  else
    printf "      \n${Red}☠ | ${URed}VIA_Team_Notest_*.zip${Red} is missing, creating now${Color_Off}\n"
    zip -r VIA_Team_Notes_$(date '+%F').zip "$cd_VIA_Team_Notes"/
  fi
}

VIA_Portable_Apps() {
  start_VIA_Portable_Apps=`date +%s`
  printf "   \n${Green}Preforming "$script" on ${UGreen}VIA_Portable_Apps${Color_Off}\n"
  cd "$cd_VIA_Portable_Apps"
  if [[ $(git remote show origin) =~ "out of date" ]]
  then
    printf "      \n${Red}☠ | Remote is out of date with ${URed}VIA_Portable_Apps${Color_Off}\n"
    rm -rf "$cd_backups"/VIA_Portable_Apps_*.zip
    git pull -v
  elif [[ $(git status) =~ "added to commit" ]]
  then
    printf "      \n${Red}☠ | ${URed}VIA_Portable_Apps${Red} is out of date with Remote${Color_Off}\n"
    rm -rf "$cd_backups"/VIA_Portable_Apps_*.zip
    git add -v .
    git commit -m "$(date '+%F_%H-%M')"
    git push -v --progress
    if [[ $(grep "$fatal_1" "$cd_temp"/"$script".log) ]]
    then
      printf "   \n${Yellow}"$script" encountered the following error with ${UYellow}VIA_Portable_Apps${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_1}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date '+%F_%H-%M'` & exited unexpectedly during VIA_Portable_Apps after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_2" "$cd_temp"/"$script".log) ]]
    then
      printf "   \n${Yellow}"$script" encountered the following error with ${UYellow}VIA_Portable_Apps${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_2}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date '+%F_%H-%M'` & exited unexpectedly during VIA_Portable_Apps after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_3" "$cd_temp"/"$script".log) ]]
    then
      printf "   \n${Yellow}"$script" encountered the following error with ${UYellow}VIA_Portable_Apps${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_3}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date '+%F_%H-%M'` & exited unexpectedly during VIA_Portable_Apps after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    else
      end=`date +%s`
      runtime_VIA_Portable_Apps=$((end-start_VIA_Portable_Apps))
      printf "\n${UPurple}   -----   VIA_Portable_Apps finished in $runtime_VIA_Portable_Apps seconds   -----   ${Color_Off}\n"
    fi
  else
    printf "      \n${Blue}✓ | ${UBlue}VIA_Portable_Apps${Blue} is up to date${Color_Off}\n"
  fi
}

VIA_Portable_Apps_check() {
  cd "$cd_backups"
  if ls VIA_Portable_Apps_*.zip 1> /dev/null 2>&1
  then
    printf "      \n${Blue}✓ | ${UBlue}VIA_Portable_Appst_*.zip${Blue} exists${Color_Off}\n"
  else
    printf "      \n${Red}☠ | ${URed}VIA_Portable_Appst_*.zip${Red} is missing, creating now${Color_Off}\n"
    zip -r VIA_Portable_Apps_$(date '+%F').zip "$cd_VIA_Portable_Apps"/
  fi
}

Documents() {
  start_Documents=`date +%s`
  printf "   \n${Green}Preforming "$script" on ${UGreen}Documents${Color_Off}\n"
  cd "$cd_Documents"
  if [[ $(git remote show origin) =~ "out of date" ]]
  then
    printf "      \n${Red}☠ | Remote is out of date with ${URed}Documents${Color_Off}\n"
    git pull -v
  elif [[ $(git status) =~ "added to commit" ]]
  then
    printf "      \n${Red}☠ | ${URed}Documents${Red} is out of date with Remote${Color_Off}\n"
    git add -v .
    git commit -m "$(date '+%F_%H-%M')"
    git push -v --progress
    if [[ $(grep "$fatal_1" "$cd_temp"/"$script".log) ]]
    then
      printf "   \n${Yellow}"$script" encountered the following error with ${UYellow}Documents${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_1}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date '+%F_%H-%M'` & exited unexpectedly during Documents after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_2" "$cd_temp"/"$script".log) ]]
    then
      printf "   \n${Yellow}"$script" encountered the following error with ${UYellow}Documents${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_2}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date '+%F_%H-%M'` & exited unexpectedly during Documents after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_3" "$cd_temp"/"$script".log) ]]
    then
      printf "   \n${Yellow}"$script" encountered the following error with ${UYellow}VIA_Portable_Apps${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_3}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date '+%F_%H-%M'` & exited unexpectedly during VIA_Portable_Apps after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    else
      end=`date +%s`
      runtime_Documents=$((end-start_Documents))
      printf "\n${UPurple}   -----   Documents finished in $runtime_Documents seconds   -----   ${Color_Off}\n"
    fi
  else
    printf "      \n${Blue}✓ | ${UBlue}Documents${Blue} is up to date${Color_Off}\n"
  fi
}

Downloads() {
  start_Downloads=`date +%s`
  printf "   \n${Green}Preforming "$script" on ${UGreen}Downloads${Color_Off}\n"
  cd "$cd_Downloads"
  if [[ $(git remote show origin) =~ "out of date" ]]
  then
    printf "      \n${Red}☠ | Remote is out of date with ${URed}Downloads${Color_Off}\n"
    git pull -v
  elif [[ $(git status) =~ "added to commit" ]]
  then
    printf "      \n${Red}☠ | ${URed}Downloads${Red} is out of date with Remote${Color_Off}\n"
    git add -v .
    git commit -m "$(date '+%F_%H-%M')"
    git push -v --progress
    if [[ $(grep "$fatal_1" "$cd_temp"/"$script".log) ]]
    then
      printf "   \n${Yellow}"$script" encountered the following error with ${UYellow}Downloads${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_1}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date '+%F_%H-%M'` & exited unexpectedly during Downloads after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_2" "$cd_temp"/"$script".log) ]]
    then
      printf "   \n${Yellow}"$script" encountered the following error with ${UYellow}Downloads${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_2}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date '+%F_%H-%M'` & exited unexpectedly during Downloads after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_3" "$cd_temp"/"$script".log) ]]
    then
      printf "   \n${Yellow}"$script" encountered the following error with ${UYellow}VIA_Portable_Apps${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_3}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date '+%F_%H-%M'` & exited unexpectedly during VIA_Portable_Apps after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    else
      end=`date +%s`
      runtime_Downloads=$((end-start_Downloads))
      printf "\n${UPurple}   -----   Downloads finished in $runtime_Downloads seconds   -----   ${Color_Off}\n"
    fi
  else
    printf "      \n${Blue}✓ | ${UBlue}Downloads${Blue} is up to date${Color_Off}\n"
  fi
}

Public() {
  start_Public=`date +%s`
  printf "   \n${Green}Preforming "$script" on ${UGreen}Public${Color_Off}\n"
  cd "$cd_Public"
  if [[ $(git remote show origin) =~ "out of date" ]]
  then
    printf "      \n${Red}☠ | Remote is out of date with ${URed}Public${Color_Off}\n"
    git pull -v
  elif [[ $(git status) =~ "added to commit" ]]
  then
    printf "      \n${Red}☠ | ${URed}Public${Red} is out of date with Remote${Color_Off}\n"
    git add -v .
    git commit -m "$(date '+%F_%H-%M')"
    git push -v --progress
    if [[ $(grep "$fatal_1" "$cd_temp"/"$script".log) ]]
    then
      printf "   \n${Yellow}"$script" encountered the following error with ${UYellow}Public${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_1}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date '+%F_%H-%M'` & exited unexpectedly during Public after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_2" "$cd_temp"/"$script".log) ]]
    then
      printf "   \n${Yellow}"$script" encountered the following error with ${UYellow}Public${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_2}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date '+%F_%H-%M'` & exited unexpectedly during Public after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_3" "$cd_temp"/"$script".log) ]]
    then
      printf "   \n${Yellow}"$script" encountered the following error with ${UYellow}VIA_Portable_Apps${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_3}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date '+%F_%H-%M'` & exited unexpectedly during VIA_Portable_Apps after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    else
      end=`date +%s`
      runtime_Public=$((end-start_Public))
      printf "\n${UPurple}   -----   Public finished in $runtime_Public seconds   -----   ${Color_Off}\n"
    fi
  else
    printf "      \n${Blue}✓ | ${UBlue}Public${Blue} is up to date${Color_Off}\n"
  fi
}

Linux_Subsystem() {
  start_Linux_Subsystem=`date +%s`
  printf "   \n${Green}Preforming "$script" on ${UGreen}Linux_Subsystem${Color_Off}\n"
  cd "$cd_Linux_Subsystem"
  if [[ $(git remote show origin) =~ "out of date" ]]
  then
    printf "      \n${Red}☠ | Remote is out of date with ${URed}Linux_Subsystem${Color_Off}\n"
    git pull -v
  elif [[ $(git status) =~ "added to commit" ]]
  then
    printf "      \n${Red}☠ | ${URed}Linux_Subsystem${Red} is out of date with Remote${Color_Off}\n"
    git add -v .
    git commit -m "$(date '+%F_%H-%M')"
    git push -v --progress
    if [[ $(grep "$fatal_1" "$cd_temp"/"$script".log) ]]
    then
      printf "   \n${Yellow}"$script" encountered the following error with ${UYellow}Linux_Subsystem${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_1}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date '+%F_%H-%M'` & exited unexpectedly during Linux_Subsystem after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_2" "$cd_temp"/"$script".log) ]]
    then
      printf "   \n${Yellow}"$script" encountered the following error with ${UYellow}Linux_Subsystem${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_2}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date '+%F_%H-%M'` & exited unexpectedly during Linux_Subsystem after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_3" "$cd_temp"/"$script".log) ]]
    then
      printf "   \n${Yellow}"$script" encountered the following error with ${UYellow}VIA_Portable_Apps${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_3}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date '+%F_%H-%M'` & exited unexpectedly during VIA_Portable_Apps after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    else
      end=`date +%s`
      runtime_Linux_Subsystem=$((end-start_Linux_Subsystem))
      printf "\n${UPurple}   -----   Linux_Subsystem finished in $runtime_Linux_Subsystem seconds   -----   ${Color_Off}\n"
    fi
  else
    printf "      \n${Blue}✓ | ${UBlue}Linux_Subsystem${Blue} is up to date${Color_Off}\n"
  fi
}

Portable_Repo() {
  start_Portable_Repo=`date +%s`
  printf "   \n${Green}Preforming "$script" on ${UGreen}Portable_Repo${Color_Off}\n"
  cd "$cd_Portable_Repo"
  if [[ $(git remote show origin) =~ "out of date" ]]
  then
    printf "      \n${Red}☠ | Remote is out of date with ${URed}Portable_Repo${Color_Off}\n"
    git pull -v
  elif [[ $(git status) =~ "added to commit" ]]
  then
    printf "      \n${Red}☠ | ${URed}Portable_Repo${Red} is out of date with Remote${Color_Off}\n"
    git add -v .
    git commit -m "$(date '+%F_%H-%M')"
    git push -v --progress
    if [[ $(grep "$fatal_1" "$cd_temp"/"$script".log) ]]
    then
      printf "   \n${Yellow}"$script" encountered the following error with ${UYellow}Portable_Repo${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_1}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date '+%F_%H-%M'` & exited unexpectedly during Portable_Repo after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_2" "$cd_temp"/"$script".log) ]]
    then
      printf "   \n${Yellow}"$script" encountered the following error with ${UYellow}Portable_Repo${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_2}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date '+%F_%H-%M'` & exited unexpectedly during Portable_Repo after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    elif [[ $(grep "$fatal_3" "$cd_temp"/"$script".log) ]]
    then
      printf "   \n${Yellow}"$script" encountered the following error with ${UYellow}VIA_Portable_Apps${Yellow}:${Color_Off}\n"
      printf "   ${Red}${fatal_3}${Color_Off}\n"
      printf "   ${Yellow}"$script" will now exit.${Color_Off}\n"
      end=`date +%s`
      runtime=$((end-start))
      printf "\n${UPurple}   -----   "$script" ended @ `date '+%F_%H-%M'` & exited unexpectedly during VIA_Portable_Apps after $runtime seconds   -----   ${Color_Off}\n"
      exit 1
    else
      end=`date +%s`
      runtime_Portable_Repo=$((end-start_Portable_Repo))
      printf "\n${UPurple}   -----   Portable_Repo finished in $runtime_Portable_Repo seconds   -----   ${Color_Off}\n"
    fi
  else
    printf "      \n${Blue}✓ | ${UBlue}Portable_Repo${Blue} is up to date${Color_Off}\n"
  fi
}

Cleanup-Backups() {
  printf "   \n${Green}Preforming Cleanup${Color_Off}\n"
  cd "$cd_backups"
  if [[ -e Profile_XML_Sort_*.zip ]]
  then
    true
  elif [[ -e Via_Git_and_Chef_Procedures_*.zip ]]
  then
    true
  elif [[ -e VIA_Team_Notes_*.zip ]]
  then
    true
  elif [[ -e VIA_Portable_Apps_*.zip ]]
  then
    true
  else
    rm -rf Profile_XML_Sort_*.zip
    rm -rf Via_Git_and_Chef_Procedures_*.zip
    rm -rf VIA_Team_Notes_*.zip
    rm -rf VIA_Portable_Apps_*.zip
  fi
}

# Script:
# ----------

## Start logging
{
start=`date +%s`
printf "\n${UPurple}   -----   "$script" Started @ `date '+%F_%H-%M'`   -----   ${Color_Off}\n"

## First, make room on workstation by emptying the recycle bin
empty_recycle_bin

## Download VIA-Tools from Chalk and diff/merge as needed
Download_VIA_Tools

## Sync all the Git dirs to http://vcs02lttnco.lttn.co.charter.com
Profile_XML_Sort
Profile_XML_Sort_check
Via_Git_and_Chef_Procedures
Via_Git_and_Chef_Procedures_check
VIA_Team_Notes
VIA_Team_Notes_check
VIA_Portable_Apps
VIA_Portable_Apps_check

## Sync all the Git dirs to Tom Charter
Documents
Downloads
Public
Linux_Subsystem
Portable_Repo

## Cleanup
# Cleanup-Backups

## End logging
end=`date +%s`
runtime=$((end-start))
printf "\n${UPurple}   -----   "$script" ended @ `date '+%F_%H-%M'` & Finished in $runtime seconds   -----   ${Color_Off}\n"

} 2>&1 | tee "$cd_temp"/"$log"
